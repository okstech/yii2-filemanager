<?php

namespace oks\filemanager\behaviors;

/**
 *
 * @author OKS Technologies
 *
 */

use oks\filemanager\models\Files;
use yii\behaviors\AttributeBehavior;


class InputModelBehavior extends AttributeBehavior
{
    /**
     * @var string
     */
    public $delimitr = ",";

    /**
     * @param $attribute
     * @return array|bool|Files[]
     */
    public function allFiles($attribute, $returnActiveQuery = false)
    {
        $data = $this->owner->{$attribute};
        if (strlen($data) == 0) {
            return false;
        }
        if ($data{0} == $this->delimitr) {
            $data = substr($data, 1);
        }
        if (strlen($data) == 0) {
            return false;
        }
        $data = explode($this->delimitr, $data);
        if (!is_array($data)) {
            return false;
        }
        if (!count($data)) {
            return false;
        }

        $elements = Files::find()->where(['in', Files::primaryKey()[0], $data]);
        if ($returnActiveQuery) {
            return $elements;
        }

        return $elements->all();
    }

}