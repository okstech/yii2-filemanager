<?php
namespace oks\filemanager\controllers;

use oks\filemanager\models\Files;
use Yii;
use yii\console\Controller;

/**
 * Class ConverterController
 * @package oks\filemanager\controllers
 *
 * console
 * controller map
 * oks\filemanager\controllers
 */
class ConverterController extends Controller
{
    /**
     * @var string
     */
    public $defaultAction = "file";

    /**
     * @return int
     */
    public function actionFile(){
        Files::cron();
        return 0;
    }
}